import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import net.runelite.mapping.Export;
import net.runelite.mapping.ObfuscatedGetter;
import net.runelite.mapping.ObfuscatedName;
import net.runelite.mapping.ObfuscatedSignature;

@ObfuscatedName("cm")
public class class83 {
   @ObfuscatedName("m")
   @Export("__cm_m")
   static final BigInteger __cm_m;
   @ObfuscatedName("f")
   @Export("__cm_f")
   static final BigInteger __cm_f;
   @ObfuscatedName("e")
   @ObfuscatedGetter(
      intValue = 533653621
   )
   @Export("__cm_e")
   public static int __cm_e;

   static {
      __cm_m = new BigInteger("10001", 16);
      __cm_f = new BigInteger("a913ba7b7e910820a733547dd6b8d8421d0944b7a5562e1899ca1579ec52fa003ce66ecc6266850cb2ee858c836f14dcf91978a7fe38f3cf6ed17c06b5bf5131a3f6e91ef5734de60343c39d0f6e41516506500660b943dba5d0558ba52d189ee69e2e24053c0037f5ace8c488ecb6281b257571f23ab04e1b6107e58ff9f80f2f2da605eba8676f69dcf8f8f0dfaf57fec803bbb0510136a9f58ce462ac2e4306aaa318f397757590d81a51b818326428083f867315a615da3efb07404cc0110ee683c847d673e82076686b51b4aeb3d726e9c54be260b2bfe79053e48be8962de6b44dcdacc0f95fbbac41e431a03a65dd5d497259b495d11fc5ae99fae10d", 16);
   }

   @ObfuscatedName("m")
   @ObfuscatedSignature(
      signature = "(III)I",
      garbageValue = "-1211070559"
   )
   static int method2027(int var0, int var1) {
      ItemContainer var2 = (ItemContainer)ItemContainer.itemContainers.get((long)var0);
      return var2 == null?-1:(var1 >= 0 && var1 < var2.ids.length?var2.ids[var1]:-1);
   }

   @ObfuscatedName("f")
   @ObfuscatedSignature(
      signature = "(Ljava/lang/String;I)Z",
      garbageValue = "-1468699107"
   )
   static boolean method2026(String var0) {
      if(var0 == null) {
         return false;
      } else {
         try {
            new URL(var0);
            return true;
         } catch (MalformedURLException var2) {
            return false;
         }
      }
   }

   @ObfuscatedName("o")
   @ObfuscatedSignature(
      signature = "(Lir;II)Llq;",
      garbageValue = "1413048059"
   )
   public static IndexedSprite method2028(AbstractIndexCache var0, int var1) {
      return !class179.method3643(var0, var1)?null:ServerPacket.method3663();
   }
}
